/* Copyright 2018 Joshua Mallad. All rights reserved.  
   packet.h - definition of tunnelplease packet structure
*/

#pragma once
#include <stdint.h>
#include <sys/user.h>

#define PACKET_MAX 65536
/* Maximum size of wrapped packet */
#define PAYLOAD_MAX 65483   /* 65535 - PACKET_HEADER_SZ */
/* Total size of Tunnelplease packet header */
#define PACKET_HEADER_SZ 52 /* 28 + 16 + 8 */
/* Size of the unencrypted portion of the Tunnelplease header */
#define PACKET_PLAIN_SZ 44  /* 28 + 16 */
/* The value below is hopefully low enough for us to always fit needed
headers into a packet. We need to fit the inner packet data, tunnelplease 
header, UDP header, and IPv4/IPv6 header into 1500 bytes or less. */
#define MTU_MAX 1300
/* Packets with timestamps outside of this range in seconds are ignored */
#define TIME_WINDOW 5
/* Length in bytes of preshared keys */
#define PSK_SZ 16

struct
__attribute__ ((aligned(PAGE_SIZE), packed))
packet {
	union {
		struct {
			union {
				/* Plain */
				struct {
					u8 hmac[28];
					/* Random nonce used for the counter */
					u8 ctr[16];
				};
				u8 plain[PACKET_PLAIN_SZ];
			};
			union {		
				/* Encrypted */
				struct {
					/* Timestamp for replay protection */
					uint64_t ts;
					/* Tunneled packet data */
					uint8_t data[PAYLOAD_MAX]; 
				};
				u8 crypt[PAYLOAD_MAX+8];
			};
		};
		u8 raw[PACKET_HEADER_SZ+PAYLOAD_MAX];
	};
};

union packet {
	struct {
		/*** Plain ***/
		u8 hmac[28];
		/* Random nonce used for the counter */
		u8 ctr[16];
		
		/*** Encrypted ***/
		/* Timestamp for replay protection */
		u64 ts;	
		/* Tunneled packet data */
		u8 data[PAYLOAD_MAX]; 
	};
	u8 raw[65536];
};

/* Control packet types */
enum {
	CTL_CONNECT = 0, 
};

struct control_packet {
	u8 type;
	u8 data[PAYLOAD_MAX-1]; 
};

/* First packet sent from client to server to initiate connection */
struct connect_packet {
	u8 key[PKEY_LEN]; /* Ephemeral EC public key */
	
};
