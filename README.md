Copyright 2019 Joshua Mallad

The goal of this project was to tunnel packets from my machine to a remote
server, using a protocol and implementation of my design. I also really
wanted to play with the userspace kernel crypto API, because that was a new
thing at the time. I learned a ton about network protocols and security
tradeoffs while working on this code.

Original readme is as follows:

# **Disclaimer:** I am not a cryptographer. This project is one of my first forays into writing crypto code EVER. The code in this repository has not been analyzed, verified, tested, or designed by anyone with real knowledge and experience in cryptography.

## Well-tested alternatives

The links below will take you to some well-tested, respected, secure network tunnels:

* [OpenVPN Static Key How-To](https://openvpn.net/community-resources/static-key-mini-howto/)
* [WireGuard Quick Start](https://www.wireguard.com/quickstart/)
* [VPN through SSH](https://wiki.archlinux.org/index.php/VPN_over_SSH)

### 

## Licensing

None of the files in this repository are licensed. This is entirely intentional. All files are copyrighted. All rights are reserved on the two main files implementing the tunnel, tun.c and packet.h. No rights are reserved on any of the other files. Bob Jenkins' ISAAC64 implementation was originally published in the public domain.

## About

Tunnelplease is an extremely simple obfuscating network tunnel. It uses
fast, lightweight crypto algorithms. A single pre-shared key is used for
encryption, decryption, and authentication on both sides of the link.

## Features

* Tunnels layer 3 (IPv4 and IPv6) in encrypted (AES-128-CTR) IPv4 or IPv6 UDP
  packets
* Each packet authenticated with a 28-byte SHA-224 HMAC
* Timestamp-based prevention of replay attacks
* Roaming (dynamic address support)

## Usage
```
Syntax: tun <hostname/IP address> <port> <client hostname/IP address>
            <client port> <keyfile> <device name> <keepalive interval>
            <roaming (true|false)> 
            [optional private IPv4 address] [optional private IPv6 address]
```
&lt;hostname/IP address&gt; and &lt;port&gt; may both be "any" to specify server (listening) mode. 

&lt;client hostname/IP address&gt; and &lt;client port&gt; must *always* be set. 

&lt;keyfile&gt; is expected to be a path to a 16-byte PSK on disk. A secure random PSK can be generated with the "genkey" tool included in this repository.  

&lt;device name&gt; sets the name of the new tun interface used for the connection.

&lt;keepalive interval&gt; Delay in seconds between sending randomly-sized
keepalive packets. These are intended to keep NAT rules alive in your
firewall. They can also keep your address updated with the endpoint. If
roaming is enabled on the endpoint and your public IP changes, the endpoint
will respond to the change. 

&lt;roaming (true|false)&gt; If true, service will be uninterrupted if the remote endpoint changes IP addresses. See the note about keepalive packets above. If false, the tunnel will only ever respond to packets received from the address and port specified on the command line.

[optional private IPv4 address] If set, configures an IPv4 address on the tunnel interface with a subnet size of /24. Specifiable subnet size is planned.

[optional private IPv6 address] If set, configures an IPv6 address on the tunnel interface with a subnet size of /64. Specifiable subnet size is planned.

## Example

### Initializing a connection
```
host-a $ ./genkey > psk
host-a $ scp psk host-b:/desired/destination/
host-a $ sudo ./tun host-b 9000 host-a 31415 psk tun0 60 false 10.10.10.1 fc::1
host-b $ sudo ./tun any any host-b 9000 psk tun0 0 true 10.10.10.2 fc::2
```
host-a acts as the client in this scenario. It is behind a NAT firewall, so it sends keepalive packets every 60 seconds. The server has a static IP,
so the client does not need to enable roaming.
host-b is the server. It is not behind a firewall, so keepalives are not
needed. The client host-a has a dynamic IP address, so the server
enables roaming. 

If you specify private addresses as an argument, be aware that the network size
is currently fixed to /24 for IPv4 and /64 for IPv6. There is no such limitation
if you configure addresses manually (such as with iproute2 or ifconfig.)

## Security

### Algorithm strength

I have purposely chosen faster but weaker crypto alogrithms for this project. AES-128-CTR and SHA224-HMAC are not future-proofed or quantum-safe. I have carefully reviewed the available options and I have chosen algorithms that I believe are "secure enough" for my purposes. Remember what was stated in the
disclaimer at the top of this document. 

### Key compromise

Each and every packet of a Tunnelplease connection is encrypted and authenticated *with the same 16-byte pre-shared key.* **No** key derivation, agreement, or exchange systems are used. **This means that if the key you use is compromised, EVERY PACKET YOU HAVE EVER SENT USING THIS KEY is now compromised.** It is absolutely imperative that you use application-level security (SSL/TLS, DNSSEC, etc) in conjunction with this tunnel. It is highly recommended that you generate new keys regularly and transfer them over a separate, secure medium like SSH.

### Obfuscation

Each Tunnelplease packet appears as totally random data to a third-party observer. There is no identifiable header or
handshake procedure. There is no key exchange procedure. Packet direction, size, and timing may still give away some hints about the nature of your activity - such as TCP handshake patterns or obvious bulk downloads.

### Encryption

The timestamp and inner packet are encrypted with AES-128-CTR. A random 16-byte
nonce (generated with the ISAAC64 CSPRNG) is used for the counter. 

### Authentication

A SHA-224 HMAC is used to authenticate the encrypted portion of each packet. The authentication code is generated *after* the sensitive data is encrypted. 

### Replay protection

Each packet has an encrypted timestamp used to protect against replay attacks.
If the timestamp of a received, otherwise valid packet is outside of the
acceptable window (currently 5 seconds), it is dropped and a warning is logged.

## Performance

### Tun driver

As the tunnel runs in userspace, the tun driver is used to copy packets to and from kernelspace. This incurs the high cost of multiple copies and context switches per packet. 

### In-place kernel-based crypto operations

libkcapi is used to encrypt, decrypt, and authenticate packets using Linux Kernel TLS. All operations are done in-place without
making any copies. A context switch is still required for each operation. 

### ISAAC64 random number generator

Tunnelplease uses the ISAAC64 random number generator implementation found 
[here.](https://burtleburtle.net/bob/rand/isaac.html#abstract) It *might* be faster than reading from /dev/urandom
or calling getrandom(). I don't know because I haven't actually tested it yet. It is also possible that using ISAAC64
has made my protocol less secure. I don't know because I'm not a cryptographer.

### TCP performance issues

For IPv4 tunnels, try issuing this command on the client side of the tunnel:
```
client $ sudo iptables -t mangle -A OUTPUT -o tun0 -p tcp -m tcp \
--tcp-flags SYN,RST SYN -j TCPMSS --clamp-mss-to-pmtu
```
For IPv6, replace "iptables" with "ip6tables".
Replace "tun0" with the &lt;device name&gt; you specified when creating the
tunnel.

This command limits the Max Segment Size of new TCP connections started through
the tunnel. The new limit is equivalent to the Maximum Transmission Unit of the
tunnel interface. This prevents TCP packets from being fragmented at the upper
UDP layer.

You may want to try turning this on if you see lots of TCP
retransmissions in a packet capture of the tunnel. Another good indicator may
be if tunneled websites are loading halfway and getting stuck. 

## Portability

### Linux-only

With libkcapi as a main component, Tunnelplease is currently Linux-only. 
You must have libkcapi installed and and a kernel supporting SHA-224 HMAC and AES-128-CTR crypto operations. 

### 64-bit optimized

The code heavily relies on 64-bit types and arithmetic. As such, it will likely run significantly slower on 
32-bit systems. 

## Notes

### Roaming

When roaming is set to "true", remote endpoint address can change. Servers
should set this for clients with dynamic addresses. Enabling roaming does
potentially create a vulnerability for replay attacks. There is some attempted
mitigation for this. It is explained above in the "replay protection" section.

### Key generation

The "genkey" tool included in this repository can be used to generate
a secure random 16-byte PSK for use with Tunnelplease. It takes no arguments
and outputs to stdout.

## Development

### Packet structure

```
            plain              encrypted
      non-authenticated      authenticated
       _______|______ ____________|______________
field  | HMAC | CTR | | Timestamp | Packet data |
offset | 0    | 28  | | 44        | 52          |
length | 28   | 16  | | 8         | <= 65483    |
       -------------- ---------------------------
```
While packet structures can theoretically handle packets of up to 65483 bytes in size, this will never happen in 
practice due to the Maximum Transmission Unit setting configured for the tunnel. The effective maximum size of a 
wrapped Tunnelplease inner packet is **1300** bytes.

### Debug mode

When Tunneleplease is run with the "DEBUG" environment variable set, extensive
debug information is logged for every packet to stderr. 

### Testing

Use the "debug" Makefile target when testing Tunnelplease. This builds the
tunnel with a couple of sanitizers, all warnings enabled, optimizations off,
and gdb debug symbols. 

# HIGHLY EXPERIMENTAL AND UNTESTED. DO NOT USE THIS SOFTWARE OUTSIDE OF A TESTING ENVIRONMENT. FOR EDUCATIONAL PURPOSES ONLY.