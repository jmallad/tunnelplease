# Copyright 2018 Joshua Mallad. No rights reserved. #

CC := gcc
CFLAGS_FAST := -march=native -g0 -O3 -fstack-protector
CFLAGS_DEBUG := -ggdb -fsanitize=undefined -fsanitize=address -Werror -pedantic -pedantic-errors -Wextra -Wall -O0 -mtune=generic -fstack-protector
IN := tun.c isaac64.c log.c
OUT := tun
LIBS := pthread kcapi

# End config #
LIBS := ${LIBS:%=-l%}

.PHONY: all debug genkey 

all: genkey
	${CC} ${CFLAGS_FAST} ${IN} -o ${OUT} ${LIBS}

debug: genkey
	${CC} ${CFLAGS_DEBUG} ${IN} -o ${OUT} ${LIBS}

genkey:
	${CC} genkey.c -o genkey

