/* Copyright 2018 Joshua Mallad. All rights reserved.
   tun.c - Dead-simple encrypted IP-in-UDP tunnel
*/

#include <linux/ipv6.h>
#include <net/if.h>
#include <netinet/in.h>
#include <netinet/ip.h>
#include <linux/if_tun.h>
#include <sys/param.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <sys/ioctl.h>
#include <sys/random.h>
#include <sys/user.h>
#include <sys/wait.h>
#include <sys/sysinfo.h>
#include <sys/socket.h>
#include <netdb.h>
#include <stdio.h>
#include <fcntl.h>
#include <unistd.h>
#include <string.h>
#include <errno.h>
#include <stdint.h>
#include <stdlib.h>
#include <pthread.h>
#include <kcapi.h>
#include <termios.h>
#include <sched.h>
#include <stdatomic.h>
#include <inttypes.h>
#include <arpa/inet.h>
#include "log.h"
#include "isaac64.h"
#include "tun.h"

/* Filename passed to logging interface */
#define FILENAME "tun.c"

/*** Macro utilities ***/
/* Acquire an atomic spinlock */
#define spin_lock(_lck) while (atomic_flag_test_and_set(_lck)) { sched_yield(); }
/* Release an atomic spinlock */
#define spin_unlock(_lck) atomic_flag_clear(_lck)
/* Greatest difference of two numbers */
#define difference(a, b) (a > b ? a - b : b - a)

/* File pointer that `debug' log messages are sent to.
   This is set to NULL if the `DEBUG' environment variable is not
   set on startup */
FILE* debugfp;
/* File descriptor for tun device */
int dev;
/* File descriptor for master UDP socket */
int master;
/* Keepalive packet interval */
int keepalive_interval = 0;
/* Roaming enabled boolean */
int roaming = 0;
/* Debug mode enabled boolean */
int debug = 0;
/* Atomic spinlock on master_addr */
atomic_flag master_lock = ATOMIC_FLAG_INIT;
/* Remote address of master socket endpoint */
struct addrinfo* master_addr;
/* Local address of master socket */
struct addrinfo* client_addr;
/* Preshared key */
uint8_t psk[PSK_SZ];
/* Private IPv4 address */
char* private_addr_v4;
/* Private IPv6 address */
char* private_addr_v6;

/* (Debug utility) Output a byte stream to stderr in hex format */
static void dump_hex(void* data, size_t sz)
{
  size_t i;
  uint8_t* p = data;
  for (i = 0; i < sz; i++) {
	fprintf(stderr, "%hhx ", p[i]);
  }
  fprintf(stderr, "\n");
}

/* (Debug utility) Dump a packet to stderr in human-readable format */
static void dump_packet(struct packet* packet, size_t sz)
{
  fprintf(stderr,"--- PACKET (%lu bytes) ----\n", sz);
  dump_hex(packet, sz);
  fprintf(stderr,">> packet.hmac\n");
  dump_hex(packet->hmac, 28);
  fprintf(stderr, ">> packet.ctr\n");
  dump_hex(packet->ctr , 16);
  fprintf(stderr,">> packet.ts\n");
  dump_hex(&packet->ts, 8);
  fprintf(stderr,">> packet.data (%lu bytes)\n", sz-PACKET_HEADER_SZ);
  dump_hex(packet->data, sz-PACKET_HEADER_SZ);
  fprintf(stderr,"-----------------------------\n");
}


/* 0 for little-endian, 1 for big-endian */
static
__attribute__ ((const))
int endian_test ()
{
	union {
		uint32_t i;
		uint8_t c[4];
	} e = { 0x01000000 };
	if (e.c[0]) {
		return 1;
	}
	return 0;
}

/* Byte-swap a 64-bit value to or from little-endian on a big-endian system */
static void order64(void* in)
{
	uint64_t* p = in;
	uint8_t r[8];
	uint8_t a;
	uint8_t b;
	union {
		uint64_t u64;
		uint8_t u8[8];
	} x;
	if (endian_test()) {
		return;
	}
	memcpy(&x.u64, p, 8);
	b = 7;
	for (a = 0; a < 8; a++) {
		memcpy(&r[a], &x.u8[b], 1);
		b--;
	}
	memcpy(in, r, 8);
}

/* Allocate a new tun device */
int tun_alloc(char* dev, int* out)
{
  const char* const FUNCTION = "tun_alloc";
  struct ifreq ifr;
  struct in6_ifreq ifr6;
  struct addrinfo hints, * res;
  int fd, err, sock;

  if (!dev || !out) {
	  return -1;
  }

  memset(&ifr, 0, sizeof(ifr));

  /* Flags: IFF_TUN   - TUN device (no Ethernet headers)
   *        IFF_TAP   - TAP device
   *
   *        IFF_NO_PI - Do not provide packet information
   */
  ifr.ifr_flags = IFF_TUN|IFF_NO_PI;
  strncpy(ifr.ifr_name, dev, IFNAMSIZ-1);

  /* Allocate a new tun device */
  fd = open("/dev/net/tun", O_RDWR);
  if (fd == -1) {
	  log_error("couldn't open /dev/net/tun: %s", strerror(errno));
	  return -1;
  }
  err = ioctl(fd, TUNSETIFF, (void *) &ifr);
  if (err == -1) {
	  log_error("couldn't allocate device: %s", strerror(errno));
	  return -1;
  }
  *out = fd;
  /* Open a socket for controlling the device */
  sock = socket(PF_INET, SOCK_DGRAM, IPPROTO_IP);
  if (sock == -1) {
	  log_error("socket failed: %s", strerror(errno));
	  return -1;
  }
  /* Set the device MTU to slightly below maximum so that our 
     packet headers will always fit */
  ifr.ifr_mtu = MTU_MAX;
  err = ioctl(sock, SIOCSIFMTU, (void*) &ifr);
  if (err == -1) {
	  log_error("couldn't set device MTU: %s", strerror(errno));
	  return -1;
  }
  /* Bring the interface up */
  ifr.ifr_flags |= IFF_UP;
  /* Apply our new interface settings */
  err = ioctl(sock, SIOCSIFFLAGS, (void*) &ifr);
  if (err == -1 ) {
	  log_error("couldn't set device flags: %s", strerror(errno));
	  return -1;
  }
  
  hints.ai_family = AF_UNSPEC;
  hints.ai_socktype = SOCK_DGRAM;
  hints.ai_flags = 0;
  hints.ai_protocol = IPPROTO_IP;
  
  /* Configure IPv4 private addressing */
  if (private_addr_v4) {
	  err = getaddrinfo(private_addr_v4, "0", &hints, &res);
	  if (err == -1) {
		  log_error("getaddrinfo failed: %s", gai_strerror(err));
		  return -1;
	  }
	  memcpy(&(ifr.ifr_addr), res->ai_addr, res->ai_addrlen);
	  freeaddrinfo(res);
	  err = ioctl(sock, SIOCSIFADDR, (void*) &ifr);
	  if (err == -1) {
		  log_error("couldn't configure address: %s", strerror(errno));
		  return -1;
	  }
	  if (getaddrinfo("255.255.255.0", "0", &hints, &res)) {
		  log_error("getaddrinfo failed: %s", gai_strerror(err));
		  return -1;
	  }
	  memcpy(&(ifr.ifr_netmask), res->ai_addr, res->ai_addrlen);
	  freeaddrinfo(res);
	  err = ioctl(sock, SIOCSIFNETMASK, (void*) &ifr);
	  if (err == -1) {
		  log_error("couldn't configure netmask: %s", strerror(errno));
		  return -1;
	  }
  }
/* Retrieve interface index number */
  err = ioctl(sock, SIOGIFINDEX, &ifr);
  if (err == -1) {
	  log_error("couldn't get interface index: %s", strerror(errno));
	  return -1;
  }
  /* Close IPv4 control socket, open an IPv6 one */
  if (close(sock) == -1) {
	  log_warning("couldn't close ioctl socket: %s", strerror(errno));
	  return -1;
  }
  sock = socket(PF_INET6, SOCK_DGRAM, IPPROTO_IP);
  if (sock == -1) {
	  log_error("socket failed: %s", strerror(errno));
	  return -1;
  }
  /* Configure IPv6 private addresssing */
  if (private_addr_v6) {
	  err = inet_pton(AF_INET6, private_addr_v6, &ifr6.ifr6_addr);
	  if (err == 0) {
		  log_error("invalid IPv6 address specified");
		  return -1;
	  }
	  else if (err == -1) {
		  log_error("error converting IPv6 address: %s\n", strerror(errno));
		  return -1;
	  }
	  ifr6.ifr6_ifindex = ifr.ifr_ifindex;
	  ifr6.ifr6_prefixlen = 64;
	  err = ioctl(sock, SIOCSIFADDR, (void*) &ifr6);
	  if (err == -1) {
		  log_error("couldn't configure address: %s", strerror(errno));
		  return -1;
	  }
  }
  /* Close IPv6 control socket */
  if (close(sock) == -1) {
	  log_warning("couldn't close ioctl socket: %s", strerror(errno));
	  return -1;
  }
  return 0;
}

/* Configure the UDP transit socket */
int configure_master(struct addrinfo** master_addr, int* master,
		     char* hostname, char* port, char* client_hostname,
		     char* client_port) {
  const char* const FUNCTION = "configure_master";
  int fd, err;
  struct addrinfo hints, client_hints;

  memset(&hints, 0, sizeof(struct addrinfo));
  hints.ai_family = AF_UNSPEC;
  hints.ai_socktype = SOCK_DGRAM;
  hints.ai_flags = 0;
  hints.ai_protocol = IPPROTO_UDP;

  memcpy(&client_hints, &hints, sizeof(struct addrinfo));
  client_hints.ai_flags = AI_PASSIVE;

  /* If we are not listening as a server, resolve the address 
   of the remote endpoint */
  if (memcmp(hostname, "any", 3)) {
	  err = getaddrinfo(hostname, port, &hints, master_addr);
	  if (err) {
		  log_error("getaddrinfo failed: %s", gai_strerror(err));
		  return 1;
	  }
  }
  /* Resolve the address of the client endpoint */
  if (getaddrinfo(client_hostname, client_port, &client_hints, &client_addr)) {
	  log_error("getaddrinfo failed: %s", gai_strerror(errno));
	  freeaddrinfo(*master_addr);
	  return 1;
  }
  /* Open master socket */
  if ((fd = socket(client_addr->ai_family, SOCK_DGRAM, IPPROTO_UDP)) == -1) {
	  log_error("couldn't open master socket: %s", strerror(errno));
	  freeaddrinfo(client_addr);
	  freeaddrinfo(*master_addr);
	  return 1;
  }
  /* Disallow IPv4 traffic to IPv6 sockets */
  if (client_addr->ai_family == AF_INET6) {
	  if (setsockopt(fd, IPPROTO_IPV6, IPV6_V6ONLY,
			 &(int){1}, sizeof(int)) == -1) {
		  log_warning("couldn't set V6-only flag on master socket: %s",
			      strerror(errno));
		  return 1;
	  }
  }
  /* Bind to client address */
  if (bind(fd, client_addr->ai_addr, client_addr->ai_addrlen) == -1) {
	  log_error("couldn't bind client socket: %s", strerror(errno));
	  freeaddrinfo(client_addr);
	  freeaddrinfo(*master_addr);
	  return 1;
  }
  /* Assign master socket */
  *master = fd;
  return 0;
}

/* When in roaming mode, update the remote endpoint to a new location */
void update_master_addr(struct sockaddr_storage* addr, socklen_t addrlen) {
  const char* const FUNCTION = "update_master_addr";
  char client[46] = {0};
  char port[6] = {0};
  struct addrinfo hints = {0};
  int ret;

  hints.ai_family = AF_UNSPEC;
  hints.ai_socktype = SOCK_DGRAM;
  hints.ai_flags = 0;
  hints.ai_protocol = IPPROTO_UDP;

  /* Convert the sockaddr_storage returned from recv() into an address
     and port */
  ret = getnameinfo((void*)addr, addrlen, client, 46, port, 6,
		    NI_NUMERICHOST|NI_NUMERICSERV);
  if (ret) {
	  log_error("getnameinfo failed: %s", gai_strerror(ret));
	  return;
  }
  spin_lock(&master_lock);
  freeaddrinfo(master_addr);
  /* Resolve the address and port to a new master addrinfo struct. */
  ret = getaddrinfo(client, port, &hints, &master_addr);
  if (ret) {
	log_error("getaddrinfo failed: %s", gai_strerror(ret));
  }
  spin_unlock(&master_lock);
  return;
}

/* Send a packet down the link to the remote endpoint */
int write_packet(int fd, struct packet* packet, size_t sz) {
  const char* const FUNCTION = "write_packet";
  int bytes;
  int ret = 0;
  spin_lock(&master_lock);
  if (!master_addr) {
	spin_unlock(&master_lock);
	return 1;
  }
  if ((bytes = sendto(fd, (void*)packet, sz, MSG_CONFIRM,
		      master_addr->ai_addr, master_addr->ai_addrlen)) == -1) {
	log_error("couldn't write packet: %s", strerror(errno));
	ret++;
  }
  spin_unlock(&master_lock);
  return ret;
}

/* Write a packet to the tun device */
int read_packet(int fd, uint8_t* packet, size_t sz) {
  const char* const FUNCTION = "read_packet";
  int bytes;
  if ((bytes = write(fd, packet, sz)) == -1) {
	if (errno == EIO) {
	  log_error("couldn't read packet: interface is not up");
	}
	else {
	  log_debug("bad packet (possibly a keepalive): %s", strerror(errno));
	}
	return 1;
  }
  return 0;
}

/* Read and process a single packet from the link */
void process_incoming_packet(int dev) {
  const char* const FUNCTION = "process_incoming_packet";
  struct packet packet;
  struct sockaddr_storage addr = {0};
  socklen_t addrlen = sizeof(struct sockaddr_storage);
  int bytes, hmac_bytes, plain_bytes, message_bytes;
  uint8_t hmac[28];
  time_t local_ts;
  
  bytes = recvfrom(master, (void*)&packet, 65535, 0, (void*)&addr, &addrlen);
  if (bytes == -1) {
	log_error("receive from master socket failed: %s", strerror(errno));
	return;
  }
  if (bytes < PACKET_HEADER_SZ) {
	  log_warning("Skipping packet (too short)");
	  if (debug) {
		  dump_hex(&packet, bytes);
	  }
	  return;
  }
  /* Size of HMAC Input */
  message_bytes = bytes - PACKET_PLAIN_SZ;
  /* Size of inner packet data */
  plain_bytes = bytes - PACKET_HEADER_SZ;
  if (!plain_bytes) {
	  log_warning("Skipping packet (no data)");
	  if (debug) {
		  dump_hex(&packet, bytes);
	  }
	  return;
  }
  /* Byte-order the packet timestamp if necessary */
  order64(&packet.ts);
  /* Calculate and verify packet HMAC */
  hmac_bytes = kcapi_md_hmac_sha224((unsigned char*)psk, PSK_SZ,
				    (void*)&(packet.ts), message_bytes,
				    hmac, 28);
  if (hmac_bytes != 28) {
	  log_error("HMAC fail");
	  return;
  }
  if (debug) {
	  dump_packet(&packet, bytes);
	  fprintf(stderr, "--- HMAC INPUT (%d bytes) ---\n", message_bytes);
	  dump_hex((void*)&(packet.ts), message_bytes);
	  fprintf(stderr, "--- HMAC OUTPUT (%d bytes) ---\n", hmac_bytes);
	  dump_hex((void*)&(hmac), hmac_bytes);
  }
  if (memcmp(hmac, packet.hmac, hmac_bytes)) {
	  if (debug) {
		  fprintf(debugfp, "--- HMAC PACKET (%d bytes) ---\n",
			  hmac_bytes);
		  dump_hex((void*)&(packet.hmac), hmac_bytes);
	  }
	  log_error("HMAC invalid");
	  return;
  }
  /* Decrypt packet starting from the timestamp */
  if (kcapi_cipher_dec_aes_ctr((unsigned char*)psk, PSK_SZ,
			       (unsigned char*)&(packet.ts),
			       message_bytes, packet.ctr,
			       (unsigned char*)&(packet.ts),
			       message_bytes) != message_bytes) {
	log_error("decrypt fail");
	return;
  }
  if (debug) {
	  dump_packet(&packet, bytes);
  }
  /* Validate packet timestamp */
  local_ts = time(NULL);
  if (difference(local_ts, (time_t)packet.ts) > TIME_WINDOW) {
	  log_error("Ignoring packet with invalid timestamp");
	  return;
  }
  /* Handle initial connection */ 
  if (!master_addr) {
	log_info("receiving initial connection\n");
	update_master_addr(&addr, addrlen);
  }
  /* Evaluate source address of packet and check for roaming */
  else if (memcmp(&addr, master_addr->ai_addr, addrlen)) {
	if (!roaming) {
	  log_warning("received valid packet from invalid address but roaming is disabled");
	  return;
	}
	log_info("detected remote endpoint roaming");
	update_master_addr(&addr, addrlen);
  }
  /* Pass the packet to the kernel */
  read_packet(dev, packet.data, plain_bytes);
}

/* Wait for and transmit a single packet down the link */
void process_outgoing_packet(int dev, void* isaac_state) {
  const char* const FUNCTION = "process_outgoing_packet";
  struct packet packet;
  int bytes, hmac_bytes, message_bytes, packet_bytes;
  /* We address the 16-byte counter array with an 8-byte pointer
     to easily generate a new random nonce with two calls to rand64() */
  uint64_t ctr;
  bytes = read(dev, packet.data, PAYLOAD_MAX);
  if (bytes == -1) {
	log_error("read packet from device failed: %s", strerror(errno));
	return;
  }
  /* Size of payload to be encrypted (starting from the packet timestamp) */
  message_bytes = bytes+8;
  /* Total size of wrapped packet */
  packet_bytes = bytes+PACKET_HEADER_SZ;
  /* Generate random nonce as explained above */
  ctr = rand64(isaac_state);
  memcpy(packet.ctr, &ctr, 8);
  ctr = rand64(isaac_state);
  memcpy(&packet.ctr[8], &ctr, 8);
  /* Apply timestamp */
  packet.ts = time(NULL);
  if (debug) {
	  dump_packet(&packet, packet_bytes);
  }
  /* Encrypt the packet and it's timestamp */
  if (kcapi_cipher_enc_aes_ctr((unsigned char*)psk, PSK_SZ,
			       (unsigned char*)&(packet.ts), message_bytes,
			       packet.ctr, (unsigned char*)&(packet.ts),
			       message_bytes) != message_bytes) {
	log_error("encrypt fail");
	return;
  }
  /* Calculate the HMAC */
  if ((hmac_bytes = kcapi_md_hmac_sha224((unsigned char*)psk, PSK_SZ,
					 (void*)&(packet.ts), message_bytes,
					 packet.hmac, 28)) != 28) {
	log_error("HMAC fail");
	return;
  }
  if (debug) {
	fprintf(stderr, "--- HMAC INPUT (%d bytes) ---\n", message_bytes);
	dump_hex((void*)&(packet.ts), message_bytes);
	fprintf(stderr, "--- HMAC OUTPUT (%d bytes) ---\n", hmac_bytes);
	dump_hex((void*)&(packet.hmac), hmac_bytes);
	fprintf(stderr, "------------------------------\n");
	dump_packet(&packet, packet_bytes);
  }
  /* Byte-order the packet timestamp if necessary */
  order64(&packet.ts);
  /* Write the wrapped packet to the socket */
  write_packet(master, &packet, packet_bytes);
}

/* Process outgoing packets from the tun device */
void* outgoing_loop(void* devp) {
  const char* const FUNCTION = "read_loop";
  void* isaac_state = alloc_isaac_state();
  if (!isaac_state) {
	log_error("couldn't allocate space for ISAAC state");
	return NULL;
  }
  if (isaac64_init(isaac_state)) {
	log_error("couldn't initialize ISAAC state");
	return NULL;
  }
  while (1) {
	process_outgoing_packet(*(int*)devp, isaac_state);
  }
  return NULL;
}

/* Process incoming packets from the master socket */
void* incoming_loop(void* devp) {
  while (1) {
	process_incoming_packet(*(int*)devp);
  }
  return NULL;
}

/* Send keepalive packets */
void* keepalive_loop() {
	const char* const FUNCTION = "keepalive_loop";
	struct packet packet;
	void* isaac_state;
	uint64_t ctr;
	uint8_t randsz;
	int message_bytes;
	int packet_bytes;
	/* Initialize random number generator */
	isaac_state = alloc_isaac_state();
	if (!isaac_state) {
		log_error("couldn't allocate space for ISAAC state");
		return 0;
	}
	if (isaac64_init(isaac_state)) {
		log_error("couldn't initialize ISAAC state");
		free(isaac_state);
		return 0;
	}
	while (1) {
		/* Generate random nonce */
		ctr = rand64(isaac_state);
		memcpy(packet.ctr, &ctr, 8);
		ctr = rand64(isaac_state);
		memcpy(&packet.ctr[8], &ctr, 8);
		/* Generate some random packet data */
		randsz = rand8(isaac_state);
		randfill(isaac_state, packet.data, randsz);
		/* Apply timestamp */
		packet.ts = time(NULL);
		/* Size of HMAC and encryption input */
		message_bytes = randsz + 8;
		/* Total size of packet */
		packet_bytes = randsz + PACKET_HEADER_SZ;
		/* Encrypt the packet and it's timestamp */
		if (kcapi_cipher_enc_aes_ctr((unsigned char*)psk, PSK_SZ,
					     (unsigned char*)&(packet.ts),
					     message_bytes, packet.ctr,
					     (unsigned char*)&(packet.ts),
					     message_bytes) != message_bytes) {
			log_error("encrypt fail");
			free(isaac_state);
			return NULL;
		}
		/* Calculate the HMAC */
		if (kcapi_md_hmac_sha224((unsigned char*)psk, PSK_SZ,
					 (void*)&(packet.ts), message_bytes,
					 packet.hmac, 28) != 28) {
			log_error("HMAC fail");
			free(isaac_state);
			return NULL;
		}
		/* Byte-order the packet timestamp if necessary */
		order64(&packet.ts);
		/* Write the wrapped packet to the socket */
		write_packet(master, &packet, packet_bytes);
		/* Randomized delay */
		sleep(keepalive_interval + rand8(isaac_state));
	}
	return NULL;
}

int main(int argc, char** argv) {
  const char* const FUNCTION = "main";
  char* remote_host;
  char* remote_port;
  char* local_host;
  char* local_port;
  char* keyfile;
  char* ifname;
  char* keepalive;
  char* roam;
  pthread_t rtid, ktid;
  if (argc < 9) {
	fprintf(stderr,
		"Syntax: tun <hostname/IP address> <port>\n"
		"\t<client hostname/IP address> <client port>\n"
		"\t<keyfile> <device name> <keepalive interval>\n"
		"\t<roaming (true|false)>\n"
		"\t[private ipv4 address] [private IPv6 address]\n");
	return 1;
  }
  remote_host = argv[1];
  remote_port = argv[2];
  local_host = argv[3];
  local_port = argv[4];
  keyfile = argv[5];
  ifname = argv[6];
  keepalive = argv[7];
  roam = argv[8];
  if (argc >= 9) {
	private_addr_v4 = argv[9];
  }
  if (argc >= 10) {
	private_addr_v6 = argv[10];
  }

  /* Messages logged at the `debug' level will only be visible when 
     the environment variable "DEBUG" is set */
  if (getenv("DEBUG")) {
	debug = 1;
	debugfp = stderr;
  }
  else {
	debugfp = NULL;
  }

  /* Read the PSK */
  int keyfd = open(keyfile, O_RDONLY);
  memset(psk, 0, PSK_SZ);
  if (read(keyfd, psk, PSK_SZ) != PSK_SZ) {
	log_fatal("error reading PSK");
	return 1;
  }
  if (close(keyfd) == -1) {
	log_fatal("error closing PSK descriptor");
	return 1;
  }

  /* Allocate the tun device */
  if (access("/dev/net/tun", R_OK) == -1 ) {
	if (mknod("/dev/net/tun", 10, 200) == -1) {
	  log_fatal("couldn't create tun device: %s", strerror(errno));
	  return 1;
	}
  }
  if ((tun_alloc(ifname, &dev)) == -1) {
	log_fatal("couldn't allocate tun device: %s", strerror(errno));
	return 1;
  }
  /* Configure the UDP transport socket */
  if (configure_master(&master_addr, &master, remote_host, remote_port,
		       local_host, local_port)) {
	log_fatal("couldn't configure master socket");
	return 1;
  }
  if (!memcmp(roam, "true", 5)) {
	roaming = 1;
  }
  /* Launch a thread for keepalives if requested */
  keepalive_interval = strtol(keepalive, NULL, 10);
  if (errno == ERANGE) {
	return 1;
  }
  if (keepalive_interval) {
	if (pthread_create(&ktid, NULL, keepalive_loop, NULL)) {
	  return 1;
	}
  }
  /* Launch a thread to process outgoing packets */
  if (pthread_create(&rtid, NULL, outgoing_loop, &dev)) {
	  return 1;
  }
  /* Process incoming packets */
  incoming_loop(&dev);
  return 0;
}
